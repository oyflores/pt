const dictionaries = {

  'en': {
    USD: 'US dollars',
    RUB: 'rubles',
    EUR: 'euros',
    GBP: 'pounds',
    JPY: 'yens'
  },
  'ru': {
    USD: 'Доллар США',
    RUB: 'Рубли',
    EUR: 'Евро',
    GBP: 'Брит. фунт',
    JPY: 'Йена'
  }

};

module.exports = (localizationKey, locale) => {
  return dictionaries[locale][localizationKey];
};
