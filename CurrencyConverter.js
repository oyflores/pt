const Promise = require('bluebird');
const request = Promise.promisify(require('request'));

const getRates = (baseCurrencyCode) =>
  request({url: `http://api.fixer.io/latest?base=${baseCurrencyCode}`, json: true})
    .then(response => {
      return response.body.rates;
    });

class CurrencyConverter {

  constructor(rates) {
    this._rates = rates;
  }

  convert(amount, toCurrCode) {
    return this._rates[toCurrCode] * Number(amount);
  }

}

module.exports = (baseCurrencyCode) => {
  return getRates(baseCurrencyCode)
    .then(rates => new CurrencyConverter(rates));
};