const printResult = console.log.bind(console, 'RES:');

const localizeTotals = totalsNotLocalized => {
  const localize = require('./localize');
  return Object.keys(totalsNotLocalized).reduce((acc, priceKey) => {
    acc[`${localize(priceKey, 'en')}`] = totalsNotLocalized[priceKey];
    return acc;
  }, {})
};

/**
 * Not efficient approach: 1. convert each price to other currencies 2. summarize by each currency
 * @param sampleCart
 * @returns {*}
 */
const main1 = (sampleCart) => {
  const loadConverter = require('./CurrencyConverter');
  const SUPPORTED_CURRENCIES = ['EUR', 'RUB', 'GBP', 'JPY'];
  const BASE_CURRENCY = 'USD';
  return loadConverter(BASE_CURRENCY)
    .then(converter => sampleCart.map(productItem => {
        const predefinedPrice = {};
        predefinedPrice[BASE_CURRENCY] = productItem.price;
        return SUPPORTED_CURRENCIES.reduce((acc, curr) => {
          acc[curr] = converter.convert(productItem.price, curr);
          return acc;
        }, predefinedPrice);
      })
        .reduce((accProducts, productItemLocalized) => {
          [BASE_CURRENCY, ...SUPPORTED_CURRENCIES].forEach(curr => {
            accProducts[curr] = (Number(accProducts[curr]) || 0) + productItemLocalized[curr];
          });
          return accProducts;
        }, {})
    );
};

/**
 * More efficient: find sum in base currency, then convert it to other currencies.
 * @param sampleCart
 * @returns {*}
 */
const main2 = (sampleCart) => {
  const loadConverter = require('./CurrencyConverter');
  const SUPPORTED_CURRENCIES = ['EUR', 'RUB', 'GBP', 'JPY'];
  const BASE_CURRENCY = 'USD';
  return loadConverter(BASE_CURRENCY)
    .then(converter => {
      const baseCurrencySum = sampleCart.reduce((acc, productItem) => {
        return acc + productItem.price;
      }, 0);
      const res = {};
      res[BASE_CURRENCY] = baseCurrencySum;
      return Object.assign(res, SUPPORTED_CURRENCIES.reduce((acc, curr) => {
          acc[curr] = converter.convert(baseCurrencySum, curr);
          return acc;
        }, {})
      )
    })
};

// main1(require('./sampleCart')).then(localizeTotals).catch(err => {
//   console.error('Could not calculate the sum because of: ', err);
//   return {};
// }).then(printResult);

main2(require('./sampleCart')).then(localizeTotals).catch(err => {
  console.error('Could not calculate the sum because of: ', err.message);
  return {};
}).then(printResult);

